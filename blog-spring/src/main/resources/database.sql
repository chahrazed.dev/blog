-- Active: 1709545600336@@127.0.0.1@3306@blog

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS post;
-- DROP TABLE IF EXISTS category;
-- DROP TABLE IF EXISTS user_post;

-- CREATE TABLE category (
--     id INT PRIMARY KEY AUTO_INCREMENT,
--     name VARCHAR(255)
-- );

CREATE TABLE post (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255),
    content VARCHAR(5000),
    author VARCHAR(255),
    post_likes INT,
    date DATETIME,
    image VARCHAR(255)
    -- id_category INT,
    -- FOREIGN KEY (id_category) REFERENCES category(id)
);

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, -- Pas obligatoire de mettre UNIQUE, pasque le check sera aussi fait par Spring
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);


-- CREATE TABLE user_post (
--     id_user_post INT PRIMARY KEY AUTO_INCREMENT,
--     id_user INT,
--     id_post INT,
--     FOREIGN KEY (id_user) REFERENCES user(id),
--     FOREIGN KEY (id_post) REFERENCES post(id)
-- );
CREATE TABLE comment (
    id INT PRIMARY KEY AUTO_INCREMENT,
    author VARCHAR(255),
    content TEXT,
    comment_likes INT,
    date DATETIME,
    id_post INT,
    FOREIGN KEY (id_post) REFERENCES post(id)
);

-- INSERT INTO category (name) VALUES
-- ('Thriller'),
-- ('Murder Mystery');

INSERT INTO post (title, content, author, date, image) VALUES
('Review: The Girl with the Dragon Tattoo', 'The Girl with the Dragon Tattoo is a psychological thriller novel by Swedish author Stieg Larsson.', "author1", NOW(), 'http://localhost:8080/dragontat.jpg'),
('Top 10 Must-Read Thriller Books', 'Discover the top 10 thriller books that will keep you on the edge of your seat.',  "author 2",  NOW(), 'http://localhost:8080/thriller.jpg'),
('Author Spotlight: Agatha Christie', 'Learn more about the "Queen of Mystery" and her iconic detective novels.',"author 3",  NOW(),  'http://localhost:8080/agatha.jpg'),
('New Release: The Silent Patient', 'The Silent Patient is a psychological thriller novel by Alex Michaelides, captivating readers with its shocking twists.', "author 4", NOW(), 'http://localhost:8080/silent-patient.webp'),
('Exploring the Dark Side of Human Nature', 'Delve into the depths of human psychology with these gripping reads that explore the darker aspects of our minds.',  "author 5",  NOW(), 'http://localhost:8080/darkside.jpg'),
('Unraveling the Mystery of Sherlock Holmes', 'Discover the genius of Sir Arthur Conan Doyle and his iconic detective, Sherlock Holmes.',"author 6",  NOW(),  'http://localhost:8080/sherlock.webp'),
('A Journey into Suspense: The Haunting of Hill House', 'Experience the chilling tale of Hill House and its mysterious inhabitants in this haunting novel by Shirley Jackson.', "author 7", NOW(), 'http://localhost:8080/haunting.jpg'),
('The Classic Thriller: Psycho', 'Psycho is a classic thriller novel by Robert Bloch, known for its spine-tingling suspense and psychological horror.', "author 8", NOW(), 'http://localhost:8080/psycho.jpg'),
('The Allure of Crime Fiction', 'Explore the allure of crime fiction and its enduring popularity among readers worldwide.', "author 9", NOW(), 'http://localhost:8080/crimefiction.webp'),
('In the Mind of a Serial Killer', 'Take a chilling journey into the mind of a serial killer with these gripping novels that delve into the psychology of crime.', "author 10", NOW(), 'http://localhost:8080/serialkiller.png'),
('Mystery and Intrigue: The Secret History', 'The Secret History is a captivating novel by Donna Tartt, blending mystery, intrigue, and psychological suspense.', "author 11", NOW(), 'http://localhost:8080/secrethistory.webp'),
('The Thrill of the Unknown: Horror Novels to Keep You Up at Night', 'Experience the thrill of the unknown with these spine-chilling horror novels that will leave you terrified to turn off the lights.', "author 12", NOW(), 'http://localhost:8080/horror.webp'),
('Exploring the Depths of the Human Psyche', 'Embark on a journey into the depths of the human psyche with these thought-provoking psychological thrillers.', "author 13", NOW(), 'http://localhost:8080/psyche.webp');

INSERT INTO comment (author, content, comment_likes, date, id_post) VALUES
('Emily', "The Girl with the Dragon Tattoo is one of the most gripping books I have ever read", 5, NOW(), 1),
('Daniel', "I've read several books from your top 10 list, and they were all fantastic recommendations.", 3, NOW(), 2),
('Sophia', 'Agatha Christie remains one of my favorite authors of all time. Her mysteries are timeless!', 7, NOW(), 3);
