package com.blog.ch.blog.entity;

import java.time.LocalDateTime;

public class Posts {
    private Integer id;
    private String author;
    private String title;
    private String content;
    private LocalDateTime date;
    private String image;
  


    public Posts() {
    }


    public Posts(String author, String title, String content, Integer postLikes, LocalDateTime date, String image,
            Category category) {
        this.author = author;
        this.title = title;
        this.content = content;
        this.date = date;
        this.image = image;
    }


    public Posts(Integer id, String author, String title, String content, Integer postLikes, LocalDateTime date,
            String image, Category category) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.content = content;
        this.date = date;
        this.image = image;
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getAuthor() {
        return author;
    }


    public void setAuthor(String author) {
        this.author = author;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }




    public LocalDateTime getDate() {
        return date;
    }


    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public String getImage() {
        return image;
    }


    public void setImage(String image) {
        this.image = image;
    }



}

