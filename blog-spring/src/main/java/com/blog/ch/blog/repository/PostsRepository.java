package com.blog.ch.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blog.ch.blog.entity.Posts;

@Repository
public class PostsRepository {

    @Autowired
    private DataSource dataSource;

    private void getPosts(List<Posts> list, ResultSet result) throws SQLException {
        Posts posts = new Posts();
        posts.setId(result.getInt("id"));
        posts.setAuthor(result.getString("author"));
        posts.setTitle(result.getString("title"));
        posts.setContent(result.getString("content"));
        posts.setDate(result.getTimestamp("date").toLocalDateTime());
        posts.setImage(result.getString("image"));

        list.add(posts);
    }

    public List<Posts> findAll() {
        List<Posts> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM post");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                getPosts(list, result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public boolean create(Posts post) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO post (author, title, content, date, image) VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, post.getAuthor());
            stmt.setString(2, post.getTitle());
            stmt.setString(3, post.getContent());
            stmt.setTimestamp(4, Timestamp.valueOf(post.getDate()));
            stmt.setString(5, post.getImage());
            stmt.executeUpdate();

            if (stmt.executeUpdate() == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                post.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error creating article", e);
        }
        return false;
    }

    public Posts findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM post WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Posts post = new Posts();
                post.setId(result.getInt("id"));
                post.setAuthor(result.getString("author"));
                post.setTitle(result.getString("title"));
                post.setContent(result.getString("content"));
                post.setDate(result.getTimestamp("date").toLocalDateTime());
                post.setImage(result.getString("image"));

                return post;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error retrieving article by ID", e);
        }
        return null;
    }

    public boolean update(Posts post) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE post SET author = ?, title = ?, content = ?,  date = ?, image = ? WHERE id = ?");
            stmt.setString(1, post.getAuthor());
            stmt.setString(2, post.getTitle());
            stmt.setString(3, post.getContent());
            stmt.setTimestamp(4, Timestamp.valueOf(post.getDate()));
            stmt.setString(5, post.getImage());
            stmt.setInt(6, post.getId());
            stmt.executeUpdate();
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error updating article", e);
        }
        return false;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM post WHERE id = ?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate() == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error deleting article", e);
        }
        return false;
    }

}