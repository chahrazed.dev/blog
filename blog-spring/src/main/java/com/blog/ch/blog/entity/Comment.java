package com.blog.ch.blog.entity;

import java.time.LocalDateTime;

public class Comment {
    private Integer id;

   

    private String author;
    private String content;
    private LocalDateTime date;

   
    private Posts post;


    public Comment() {
    }


    public Comment(String author, String content, LocalDateTime date, Posts post) {
        this.author = author;
        this.content = content;
        this.date = date;
        this.post = post;
    }


    public Comment(Integer id, String author, String content, LocalDateTime date, Posts post) {
        this.id = id;
        this.author = author;
        this.content = content;
        this.date = date;
        this.post = post;
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getAuthor() {
        return author;
    }


    public void setAuthor(String author) {
        this.author = author;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public LocalDateTime getDate() {
        return date;
    }


    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public Posts getPost() {
        return post;
    }


    public void setPost(Posts post) {
        this.post = post;
    }

    
    
}
