package com.blog.ch.blog.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.blog.ch.blog.entity.Posts;
import com.blog.ch.blog.repository.PostsRepository;

@CrossOrigin ("*")
@RestController
@RequestMapping("/api/posts")
public class PostsController {

    @Autowired
    private PostsRepository postsRepository;

    @GetMapping
    public List<Posts> getAllPosts() {
        return postsRepository.findAll();
    }

    @GetMapping("/{id}")
    public Posts getPostById(@PathVariable int id) {
        Posts post = postsRepository.findById(id);
        if (post == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return post;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Posts addPosts(@RequestBody Posts post) {
        post.setDate(LocalDateTime.now()); 
        postsRepository.create(post);
        return post;
    }

    @PutMapping("/{id}")
    public Posts updatePost(@PathVariable int id, @RequestBody Posts post) {
        getPostById(id);
        post.setId(id);
        postsRepository.update(post);
        return post;
    }
    

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePost(@PathVariable int id) {
       getPostById(id);
       postsRepository.delete(id);
    }
}