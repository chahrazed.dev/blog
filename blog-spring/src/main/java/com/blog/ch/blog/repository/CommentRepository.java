package com.blog.ch.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blog.ch.blog.entity.Comment;

@Repository
public class CommentRepository {

    @Autowired
    private DataSource dataSource;

    private void getComments(List<Comment> list, ResultSet result) throws SQLException {
        Comment comments = new Comment();
        comments.setId(result.getInt("id"));
        comments.setAuthor(result.getString("author"));
        comments.setContent(result.getString("content"));
        comments.setDate(result.getTimestamp("date").toLocalDateTime());

        list.add(comments);
    }

    public List<Comment> findAll() {

        List<Comment> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM comments");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                getComments(list, result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public boolean create(Comment comment) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO comments (author, content, date, post_id) VALUES (?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, comment.getAuthor());
            stmt.setString(2, comment.getContent());
            stmt.setTimestamp(3, Timestamp.valueOf(comment.getDate()));
            stmt.setInt(4, comment.getPost().getId());
            stmt.executeUpdate();
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error creating article", e);
        }
        return false;
    }

    public Comment findById(int id) {
        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Comment comment = new Comment();
                comment.setId(result.getInt("id"));
                comment.setAuthor(result.getString("author"));
                comment.setContent(result.getString("content"));
                comment.setDate(result.getTimestamp("date").toLocalDateTime());

                return comment;

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error retrieving article by ID", e);
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM comments WHERE id = ?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate() == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error deleting article", e);
        }
        return false;
    }

}
