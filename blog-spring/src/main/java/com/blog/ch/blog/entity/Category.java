package com.blog.ch.blog.entity;

import java.util.List;

public class Category {
    private Integer id;
    private String name;
    private List<Posts> articles;

    public Category() {
    }

    public Category(String name, List<Posts> articles) {
        this.name = name;
        this.articles = articles;
    }

    public Category(Integer id, String name, List<Posts> articles) {
        this.id = id;
        this.name = name;
        this.articles = articles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Posts> getArticles() {
        return articles;
    }

    public void setArticles(List<Posts> articles) {
        this.articles = articles;
    }
    
}
