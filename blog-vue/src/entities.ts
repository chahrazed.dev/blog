export interface Post {
    id?: number;
    title: string;
    author?: string;
    content: string;
    // postLike?: number;
    date?: number;
    image?: string;
    postComment?: number;
    // category?: string;
    // post: Post[];
}

export interface Category {
    id?: number;
    name: string;
}



export interface Comment {
    id?: number;
    author?: string;
    content: string;
    date?: number;
}

export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
}