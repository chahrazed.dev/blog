import PostForm from '@/components/PostForm.vue'
import AboutView from '@/views/AboutView.vue'
import ContactView from '@/views/ContactView.vue'
import LoginView from '@/views/LoginView.vue'
import PostDetailsView from '@/views/PostDetailsView.vue'
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CreateUser from '@/components/CreateUser.vue'
import AOS from "aos";
import 'aos/dist/aos.css';



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/post/:id',
      name: 'PostDetails',
      component: PostDetailsView,
      props: true

    },
    // {
    //   path: '/post/edit/:id',
    //   name: 'EditPost',
    //   component: PostEdit
    // },

    {
      path: '/posts/create',
      name: 'CreatePost',
      component: PostForm
    },

    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/contact',
      name: 'contact',
      component: ContactView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/signup',
      name:'signup',
      component: CreateUser
    }
  ]
})

export default router
