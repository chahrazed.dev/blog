import axios from "axios";
import type { Post } from "@/entities";

export async function fetchPosts() {
    const response = await axios.get<Post[]>(
        "http://localhost:8080/api/posts"
    );
    return response.data;
}

export async function fetchPostByID(id:any) {
    const response = await axios.get<Post>(
        `http://localhost:8080/api/posts/${id}`
    );
    return response.data;
}

export async function createPost(post: Post){
    const response = await axios.post<Post>(
        "http://localhost:8080/api/posts",
        post
    );
    return response.data;
}

export async function updatePost(post: Post) {
    const response = await axios.put<Post>(
        `http://localhost:8080/api/posts/${post.id}`,
        post
    );
    return response.data;
}

export async function deletePost(id:any){
    await axios.delete(`http://localhost:8080/api/posts/${id}`);
}