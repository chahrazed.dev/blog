
import axios from 'axios';
import type { Category } from '@/entities';

const BASE_URL = 'http://localhost:8080/api/categories';

export async function fetchCategories(): Promise<Category[]> {
    const response = await axios.get<Category[]>(BASE_URL);
    return response.data;
}

export async function createCategory(category: Category): Promise<Category> {
    const response = await axios.post<Category>(BASE_URL, category);
    return response.data;
}

export async function updateCategory(category: Category): Promise<Category> {
    const response = await axios.put<Category>(`${BASE_URL}/${category.id}`, category);
    return response.data;
}

export async function deleteCategory(id: number): Promise<void> {
    await axios.delete(`${BASE_URL}/${id}`);
}
