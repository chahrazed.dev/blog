
import axios from 'axios';
import type { Comment } from '@/entities';

const BASE_URL = 'http://localhost:8080/api/comments';

export async function fetchComments() {
    const response = await axios.get<Comment[]>(BASE_URL);
    return response.data;
}
export async function fetchCommentsByID(id: any){
    const response = await axios.get<Comment>(`${BASE_URL}/${id}`);
    return response.data;

}
export async function createComment(comment: Comment){
    const response = await axios.post<Comment>(BASE_URL, comment);
    return response.data;
}


export async function deleteComment(id: any) {
    await axios.delete(`${BASE_URL}/${id}`);
}
