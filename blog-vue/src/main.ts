import 'bootstrap/dist/css/bootstrap.css';
import './assets/main.css';
import AOS from 'aos';
import 'aos/dist/aos.css';


import './axios-config';
import { createPinia } from 'pinia';



import { createApp } from 'vue';
import App from './App.vue'; 
import router from './router';

const app = createApp(App);
app.use(createPinia());

app.use(router);
app.use(AOS);
app.mount('#app');
